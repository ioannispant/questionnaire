from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    test = db.Column(db.String(32))
    answers = db.Column(db.Integer)
    time = db.Column(db.Time, index=True) ##COULD BE DATETIME

    def __repr__(self):
        return '<User {}>'.format(self.username)  

