from flask import flash, render_template, request, redirect, url_for
from werkzeug.urls import url_parse
import random, copy
from app import app, db
from app.forms import RegistrationForm
from app.models import User


DS_quiz = [
    {
        "question": "Which statistic is most resistant to outliers (robust)?",
        "answers": ['Median', 'Arithmetic mean', 'Standard deviation', 'Analysis of variance'],
        "correct": "Median"
    },
    {
        "question": "Which of these is a continuous distribution?",
        "answers": ['Lognormal distribution', 'Poisson distribution', 'Binomial distribution', 'Hyper geometric distribution'],
        "correct": 'Lognormal distribution'
    },
    {
        "question": "Which of the following step is performed by a data scientist after acquiring the data?",
        "answers": ['Data Cleansing', 'Data Integration', 'Data Replication', 'Data Analysis'],
        "correct": 'Data Cleansing'
    },
    {
        "question": "Supervised learning and unsupervised clustering both require at least one _____.",
        "answers": ['input attribute', 'hidden attribute', 'output attribute', 'categorical attribute'],
        "correct": "input attribute"
    },
    {
        "question": "What is the output of the following python code?",
        "answers": ['r', 't', 'e', 'D'],
        "picture": "python-code-ds-questionnaire.png",
        "correct": 'r'
    },
    {
        "question": "What is the time complexity of the following java code?",
        "answers": ['O(N + M)', 'O(N * M)', 'O(log(N+M))', 'O(N*log(M))'],
        "picture": "java-code-ds-questionnaire.png",
        "correct": 'O(N + M)'
    },
    {
        "question": "How can you prevent a clustering algorithm from getting stuck in bad local optima?",
        "answers": ['Use multiple random initializations', 'Set the same seed value for each run', 'Both A and B', 'None of the above'],
        "correct": 'Use multiple random initializations'

    },
    {
        "question":  "Which of the following algorithms are not an example of ensemble learning?",
        "answers": ['Decision Trees', 'Random Forest', 'Adaboost', 'Bagging K-NN'],
        "correct": 'Decision Trees'
    },
    {
        "question": "Suppose you are solving a classification problem with highly imbalanced class. The majority class is observed 99% of times in the training data. Which of the following is true in such a case?\n\t1. Accuracy metric is not a good metric for imbalanced class problems.\n\t2. It is easier to predict the minority class in highly imbalanced datasets.",
        "answers": ['Only 1', 'Only 2', '1 and 2', 'None'],
        "correct": "Only 1"
    },

    {
        "question": "Which of the following is not true about k-fold cross-validation?",
        "answers": ['It measures the performance of the model over a fixed validation set', 'It is used to evaluate machine learning models on a limited data sample', 'It is used to prevent overfitting', 'It can be used with both regression and classification models'],
        "correct": 'It measures the performance of the model over a fixed validation set'
    }
]

DE_quiz = [
    {
        "question": "Which is the combination that makes the expression (p || (r && (~ q))) && ((r || (~ p)) && q) true?",
        "answers": ['p, r, q = True, True, True','p, r, q = True, True, False','p, r, q = True, False, True','p, r, q = False, True, True']
    },
    {
        "question": "Which algorithm has the best time complexity?",
        "answers": ['Binary search','Quick sort','Selection sort','Travelling salesperson']
    },
    {
        "question": "Which data structure follows the LIFO rule when removing an item?",
        "answers": ['Stack','Queue','Tree','Array']
    },
    {
        "question": "In OOP, multiple inheritence means that ",
        "answers": ['a single class is inheriting from multiple super classes','multiple classes are inheriting from a single super class','a single class is inheriting form a single super class which is inheriting from another single super class','None of the above']
    },
    {
        "question": "Which is a layer of a general OS?",
        "answers": ['Memory Management','Applications','Hardware','All of the above']
    },
    {
        "question": "During which testing tactic versions of the software are released to an audience outside of the programming team as to ensure that the product has still a few faults or bugs?",
        "answers": ['Beta testing','Regression testing','Compatibility testing','System testing']
    },
    {
        "question": "What is the Linux command that displays the user's location in the file system?",
        "answers": ['pwd','echo','mkdir','rm']
    },
    {
        "question": "What is the output of the following program?",
        "answers": ['11 10', '11 11', '10 10', '10 11'],
        "picture": "java_code.jpg"
    },
    {
        "question": "Which SQL command finds the values that contain the letter \"a\" and the letter \"b\" is the second to last?",
        "answers": ['SELECT * FROM Customers WHERE Name LIKE \"%a%b_\"', 'SELECT * FROM Customers WHERE Name IS \"_a_b%\"', 'SELECT DISTINCT * FROM Customers WHERE Name LIKE \"_a_b%\"', 'SELECT DISTINCT * FROM Customers WHERE Name IS \"%a%b_\"']
    },

    {
        "question": "What is the output of the following code?",
        "answers": ['dlroW olleH', 'Hello Worl', 'd', 'Error'],
        "picture": "python_code.jpg",
    }
]


@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
def index():
    
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('quiz'))
    return render_template('index.html', title = "Welcome to Xomnia", form=form)

@app.route('/quiz')
def quiz():

    # Leave the order of the questions untouched, but shuffle the order of each one's answers
    questions_stable = questions

    for i in list(questions.keys()):
        random.shuffle(questions[i])

    return render_template('quiz.html', title = "Data Engineer Questionnaire", q = questions_stable, o = questions)


@app.route('/score', methods=['POST'])
def score():
    
    #Get the answers, verify how many out of them are correct

    correct = 0
    answered = []

    for j in list(questions.keys()):
        answered.append(request.form[j])

    for x in answered:
        if x == list(original_questions.items())[answered.index(x)][1][0]:
            correct = correct+1
    correct = str(correct)

    return render_template('score.html', title = "Score", c = correct)


@app.route('/quiz', methods=['GET', 'POST'])
def quiz():

    user = "DS"
    #user = "DE"

    if user == "DS":
        quiz = DS_quiz
    else:
        quiz = DE_quiz

    return render_template('quiz.html', quiz=quiz)
